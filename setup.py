import setuptools

setuptools.setup(
    name="autorunner",
    version="0.0.7",
    author="FlavKaze",
    author_email="flav.gaspareto@gmail.com",
    description="Creates an environment to run several instances of celery.",
    packages=setuptools.find_packages(),
    license='MIT',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        
    ],
    package_data={
        'autorunner': [
            'run.sh',
        ]
    },
    python_requires='>=3.8',
)