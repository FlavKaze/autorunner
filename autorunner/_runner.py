
from pprint import pprint
from datetime import datetime
from tqdm import tqdm
from time import sleep

from celery import group
import _config
from _tasks import _celery_task

import importlib
import _config
import os
import sys

try:
    runner_file = _config.runner_file
    file_split = os.path.split(runner_file)

    runner_file = file_split[1]
    sys.path.append(file_split[0])

    if runner_file.endswith(".py"):
        runner_file = runner_file[:-3]
    external_methods = importlib.import_module(runner_file)
except Exception:
    external_methods = None

class AutoRunner:

    def begin_process(self, iterable_obj):
        with tqdm(total=len(iterable_obj)) as progress:
            job = group([_celery_task.s(each) for each in iterable_obj])
            result = job.apply_async()
            progress.set_description("Celery Queue")
            while not result.successful():
                sleep(0.1)
                progress.n = result.completed_count()
                progress.refresh()
            return result.get()

    def pre_processing(self) -> list:
        if external_methods and "_pre_processing" in dir(external_methods):
            return external_methods._pre_processing()
        to_return = [num  for num in range(10)]
        return to_return

    def post_processing(self, auto_runner_return) -> None:
        if external_methods and "_post_processing" in dir(external_methods):
            return external_methods._post_processing(auto_runner_return)
        # if _config.method_name_to_run[0] != "run_all":
            output = [
                [
                    str(i.get(m))
                    for i in auto_runner_return
                ] 
                for m in _config.method_name_to_run
            ]
            print(f"Return count - {len(output)}/{len(auto_runner_return)}")

        #     with open(_config.output_path_file, "w") as f:
        #         f.write("\n".join(output))
        # else:
        pprint(auto_runner_return)

    def run(self):
        inite =  datetime.utcnow()
        my_iterable_var_here = self.pre_processing()

        auto_runner_return = self.begin_process(
            my_iterable_var_here or _config.my_iterable_var_here, 
        )
        self.post_processing(auto_runner_return)
        print(f"Execution time {datetime.utcnow() - inite}")

if __name__ == '__main__':
    AutoRunner().run()

