#! /bin/bash

#Set vars
BASEDIR=$(dirname "$0")
echo "Running in ${BASEDIR}"

if [ -z "${PATH_TO_RUN}" ]; then
    export PATH_TO_RUN=`pwd`
fi

if [ -z "${VENV}" ]; then
    VENV="venv/"
fi

if [ -z "${PRESERVE_VENV}" ]; then
    PRESERVE_VENV=true
fi

if [ -z "${CELERY_WORKER}" ]; then
    CELERY_WORKER=$1
fi

if [ -z "${MAIN_FILE}" ]; then
    export MAIN_FILE=$2
fi

if [ -z "${TO_RUN}" ]; then
    export TO_RUN=$3
fi

if [ -z "${METHOD_NAME_RUN}" ]; then
    export METHOD_NAME_RUN="${@:4}"
fi

# Set package directory
cd "${BASEDIR}"

# Cat celery config
if [ -z "${CELERY_DB}" ]; then
    CELERY_DB=`cat _config.py | grep "redis_db = " | cut -d " " -f 3`
fi
if [ -z "${CELERY_QUEUE}" ]; then
    CELERY_QUEUE=`cat _config.py | grep "CELERY_QUEUE = " | cut -d " " -f 3 | cut -d "'" -f 2`
fi

# Install redis if necessary
if ! dpkg-query -l redis > /dev/null ; then 
    echo "Redis is required!"
    sudo apt install redis -y
fi

# Create venv / activate
if [ ! -d "$VENV" ]; then
    python3.8 -m venv venv --clear
    source "venv/bin/activate"
    python3.8 -m pip install --upgrade pip
    pip3.8 install wheel
    pip3.8 install pipreqs
    pip3.8 install redis
    pip3.8 install celery==5.2.6
    pipreqs . --force
    pip3.8 install -r requirements.txt

else
    echo "${VENV} exists, activating..."
    source "${VENV}/bin/activate"
fi

# Redis start
redis-server --save "" --appendonly no >> /dev/null &
REDIS_PID=$!

# Clean redis
sleep 0.1
redis-cli -n $CELERY_DB flushdb >> /dev/null &

# Celery start
celery -q -A _tasks worker -c $CELERY_WORKER --pool=prefork -Q $CELERY_QUEUE >> /dev/null &
CELERY_PID=$!

# Script start
# if [ "$MAIN_FILE" != "" ] && [ -f "$MAIN_FILE" ] ; then
#     echo  "Run file ${MAIN_FILE}"
#     python3 "$MAIN_FILE"
# elif [ "${PATH_TO_RUN}/${MAIN_FILE}" != "" ] && [ -f "${PATH_TO_RUN}/${MAIN_FILE}" ] ; then
#     echo  "Run file ${PATH_TO_RUN}/${MAIN_FILE}"
#     python3 "${PATH_TO_RUN}/${MAIN_FILE}"
# else
echo "Run file _runner.py"
python3.8 _runner.py
# fi

# Redis stop
echo "Stoping redis..."
if ps -p $REDIS_PID | grep $REDIS_PID >> /dev/null ; then
    kill ${REDIS_PID} >> /dev/null &
fi

# Celery stop
echo "Stoping celery..."
if ps -p $CELERY_PID | grep $CELERY_PID >> /dev/null ; then
    kill -9 ${CELERY_PID} > /dev/null 2>&1
 
fi

# Remove venv
if [ "$PRESERVE_VENV" = false ]; then
    rm -r $VENV
fi

# Unset vars
unset CELERY_WORKER
unset MAIN_FILE
unset TO_RUN
unset METHOD_NAME_RUN
unset PATH_TO_RUN

# awk
# cat tasks.py | grep -e def | sed 's/def //g' | cut -d "(" -f 1
# cat tasks.py | grep -e def | sed 's/def //g' | cut -d "(" -f 1 | wc -l
# var="my_var"
# new_value="NEW VALUE"
# awk -v var="$var" -v new_val="$new_value" 'BEGIN{FS=OFS="="}match($1, "^\\s*" var "\\s*") {$2=" " new_val}1'