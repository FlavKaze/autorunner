from celery import Celery
import _config

def _create_celery_app():
    """
    Create a new Celery object and tie together the Celery config to the app's
    config. Wrap all tasks in the context of the annotators.

    :param app: Flask app
    :return: Celery app
    """
    celery = Celery("celery", broker=_config.SQS_BROKER)
    celery.conf['task_routes'] = _config.task_routes
    celery.conf["result_backend"] = _config.result_backend
    celery.conf["redis_db"] = _config.redis_db

    celery.conf.update(_config.ADDITIONAL_CONFIG)

    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery