from _celery_app import _create_celery_app
from celery.exceptions import SoftTimeLimitExceeded, TimeLimitExceeded
import importlib
import _config
import os
import sys

celery = _create_celery_app()
try:
    to_run_file = _config.to_run_file
    file_split = os.path.split(to_run_file)

    to_run_file = file_split[1]
    sys.path.append(file_split[0])

    if to_run_file.endswith(".py"):
        to_run_file = to_run_file[:-3]
    to_run = importlib.import_module(to_run_file)

except ModuleNotFoundError:
    print(f"{_config.style.RED}{_config.to_run_file} not found.")
    exit()
    # asnwer = input(f"Run to_run.py ? s/n")
    # if asnwer.lower() == "s":
    #     to_run = importlib.import_module(f'to_run')
    # else:
    #     exit()
    

@celery.task(bind=False)
def _celery_task(*args, **kwargs):
    to_return = {}
    try:
        if isinstance(_config.method_name_to_run, list):
            if isinstance(_config.method_name_to_run[0], str) and _config.method_name_to_run[0] == "run_all":
                functions_to_run = dir(to_run)
            else:
                functions_to_run = _config.method_name_to_run
        else:
            print(f"{_config.style.MAGENTA}Method not found.")
            return "Method not found."
        functions_to_run = [f for f in functions_to_run if not f.startswith("_")]
        for each in functions_to_run:
            obj = getattr(to_run, each)
            if callable(obj):
                try:
                    to_return.update({each: obj(*args, **kwargs)})
                except Exception as error:
                    print(f"{_config.style.RED}Erro ao executar - {each=} {args=} {kwargs=} {error=}")
    except (TimeLimitExceeded, SoftTimeLimitExceeded):
        print(f"{_config.style.MAGENTA}Task timeout - {each=} {args=} {kwargs=}")

    return to_return

    
