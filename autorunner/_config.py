# Celery configuration
import os

class style():
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    UNDERLINE = '\033[4m'
    RESET = '\033[0m'

redis_db = 15 
LOCAL_REDIS_URL = f'redis://localhost/{redis_db}'
REDIS_URL = os.environ.get('REDIS_URL', LOCAL_REDIS_URL)

LOCAL_BROKER_URL = f"redis://"
CELERY_BROKER_URL = LOCAL_BROKER_URL
result_backend = LOCAL_REDIS_URL
# redis_max_connections = 5
# broker_use_ssl = False

SQS_BROKER = REDIS_URL
CELERY_QUEUE = 'celery_queue'

task_routes = {
    '_tasks._celery_task': {'queue': CELERY_QUEUE},
}
ADDITIONAL_CONFIG = {
            'worker_prefetch_multiplier': 1,  # number of tasks each worker grabs from queue at once
            'task_time_limit': 604800,
            'task_soft_time_limit': 300,  # remove task from queue if not completed in 5 minutes
            'task_acks_late': False,
            'task_track_started': True,
            'result_expires': 604800,  # one week
            'task_reject_on_worker_lost': False,
            'task_queue_max_priority': 10,
            # 'broker_pool_limit': 0,
        }

path_to_run =  os.environ.get("PATH_TO_RUN", "./")
output_path_file = os.path.join(path_to_run, "runner_output.txt")
runner_file = os.environ.get('MAIN_FILE', "")
runner_file = os.path.join(path_to_run, runner_file) if runner_file else ""
to_run_file = os.environ.get('TO_RUN', None)
to_run_file = os.path.join(path_to_run, to_run_file) if to_run_file else "to_run"
# if "run_all" in index 0 run all callable inside to_run
method_name_to_run = os.environ.get('METHOD_NAME_RUN', "run_all").split(" ")

my_iterable_var_here = []
