import os

base_dir = os.path.split(os.path.realpath(__file__))[0]
file_path = os.path.join(base_dir, 'run.sh')
def play(
    CELERY_WORKER_COUNT: int,
    MAIN_FILE: str, 
    CUSTOM_FILE: str, 
    CUSTOM_METHOD: list,
    PATH_TO_RUN: str = "",
    ):
    CUSTOM_METHOD = " ".join(CUSTOM_METHOD)

    os.environ["CELERY_WORKER"] = str(CELERY_WORKER_COUNT)
    os.environ["MAIN_FILE"] = MAIN_FILE
    os.environ["TO_RUN"] = CUSTOM_FILE
    os.environ["METHOD_NAME_RUN"] = CUSTOM_METHOD
    os.environ["PATH_TO_RUN"] = PATH_TO_RUN

    try:
        os.system(f'{file_path}')
    except PermissionError:
        os.system(f"chmod +x {file_path}")
        play(CELERY_WORKER_COUNT, MAIN_FILE, CUSTOM_FILE, CUSTOM_METHOD, PATH_TO_RUN)

    del os.environ["CELERY_WORKER"]
    del os.environ["MAIN_FILE"]
    del os.environ["TO_RUN"]
    del os.environ["METHOD_NAME_RUN"]
    del os.environ["PATH_TO_RUN"]


if __name__ == "__main__":
    CELERY_WORKER_COUNT = 10
    MAIN_FILE = '_runner.py'
    CUSTOM_FILE = 'to_run.py'
    CUSTOM_METHOD = ['fib']
    PATH_TO_RUN = ""
    play(CELERY_WORKER_COUNT, MAIN_FILE, CUSTOM_FILE, CUSTOM_METHOD, PATH_TO_RUN)