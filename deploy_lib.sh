!# /bin/bash
rm -r dist autorunner.egg-info build
source venv/bin/activate
# pip3.8 install wheel
# pip3.8 install twine==4.0.0
python3 setup.py build bdist_wheel
python3 -m twine upload --repository gitlab dist/* --verbose