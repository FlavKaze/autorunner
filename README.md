for usage:

Step 1 create a .py script and implement _pre_processing and _post_processing

```
def _pre_processing() -> list:
    raise NotImplementedError

def _post_processing(autorunner_return) -> None:
    raise NotImplementedError
```

MAIN_FILE - File name with _pre_processing and _post_processing


 Step 2 create a .py script and implement you custon method

CUSTOM_FILE - File name with custom methods

CUSTOM_METHOD - custom methods name


Step 3 set you config

CELERY_WORKER_COUNT - Number of celery worker

PATH_TO_RUN - Path with yours files (default is "./")


Step 4 import auto runner and call method with paramters

```
from autorunner.play import play

play(CELERY_WORKER_COUNT, MAIN_FILE, CUSTOM_FILE, CUSTOM_METHOD, PATH_TO_RUN)
```
